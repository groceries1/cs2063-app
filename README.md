# Grocery Buddy

Grocery Buddy is an app that makes it easier to shop for groceries. It allows for building a shopping list, either by scanning barcodes or by searching for products, and makes it easy to see the total cost and weight of your cart. This is useful when sticking to a budget, or for people who walk to the store and cannot carry more than a certain weight.

When viewing a product, a graph shows the price history of the item. This makes it easy to see if it is currently on sale, or if a sale recently ended.