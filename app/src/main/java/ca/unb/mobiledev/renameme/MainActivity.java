package ca.unb.mobiledev.renameme;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.icu.number.NumberFormatter;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.text.NumberFormat;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity
{
	/**
	 * The cart/shopping list to be remembered
	 */
	Cart cart;

	private RecyclerView recyclerView;
	private TextView emptyCartView;
	private TextView totalMassView;
	private TextView totalPriceView;

	final String TAG = "Main Activity";

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		cart = new Cart(getSharedPreferences(getString(R.string.shared_cart), MODE_PRIVATE));

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		recyclerView = findViewById(R.id.rec_view);
		emptyCartView = findViewById(R.id.emptyCartView);
		totalMassView = findViewById(R.id.totalweight);
		totalPriceView = findViewById(R.id.totalprice);

		FloatingActionButton fab = findViewById(R.id.fab);
		fab.setOnClickListener(v ->
		{
			Intent newItem = new Intent(MainActivity.this, ProductSearch.class);
			startActivity(newItem);
		});

		FloatingActionButton wrench = findViewById(R.id.wrench);
		wrench.setOnClickListener(v ->
		{
			Intent newItem = new Intent(MainActivity.this, StoreSelection.class);
			startActivity(newItem);
		});

		updateEntries();
	}

	// We use this to make the view act stateless
	// So if they leave, add some items to their cart, and come back,
	// the cart refreshes
	public void onRestart()
	{
		super.onRestart();

		updateEntries();
	}

	private void updateEntries()
	{
		totalPriceView.setText(NumberFormat.getCurrencyInstance().format(cart.totalPrice()));
		totalMassView.setText(cart.totalMass() + " g");

		ArrayList entries = cart.getEntries();
		recyclerView.setAdapter(new MyAdapter(entries));

		if(entries.size() == 0)
		{
			emptyCartView.setVisibility(View.VISIBLE);
		}
		else
		{
			emptyCartView.setVisibility(View.INVISIBLE);
		}
	}

	public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder>
	{
		private ArrayList<Cart.CartEntry> mDataset;

		public MyAdapter(ArrayList<Cart.CartEntry> myDataset) {
			mDataset = myDataset;
		}

		// ViewHolder represents an individual item to display. In this case
		// it will just be a single TextView (displaying the title of a course)
		// but RecyclerView gives us the flexibility to do more complex things
		// (e.g., display an image and some text).
		public class ViewHolder extends RecyclerView.ViewHolder
		{
			public LinearLayout linearLayout;
			public TextView mNameView;
			public TextView mMassView;
			public TextView mPriceView;
			public ViewHolder(LinearLayout ll)
			{
				super(ll);

				linearLayout = ll;
				mNameView = (TextView) ll.getChildAt(0);
				mMassView = (TextView) ll.getChildAt(1);
				mPriceView = (TextView) ll.getChildAt(2);
			}
		}

		// The inflate method of the LayoutInflater class can be used to obtain the
		// View object corresponding to an XML layout resource file. Here
		// onCreateViewHolder inflates the TextView corresponding to item_layout.xml
		// and uses it to instantiate a ViewHolder.
		@Override
		public MyAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
		{
			LinearLayout ll = (LinearLayout) LayoutInflater.from(parent.getContext())
					.inflate(R.layout.item_layout, parent, false);

			return new ViewHolder(ll);
		}

		// onBindViewHolder binds a ViewHolder to the data at the specified
		// position in mDataset
		@Override
		public void onBindViewHolder(ViewHolder holder, int position)
		{

			final Cart.CartEntry item = mDataset.get(position);

			Product p = item.getProduct();

			holder.mNameView.setText(item.getQuantity() + "x " + p.getName());
			holder.mMassView.setText(p.getWeight() * item.getQuantity() + " g");
			if(p.getWeight() == 0){
				holder.mMassView.setText(getString(R.string.weight_not_available));
			}
			holder.mPriceView.setText(NumberFormat.getCurrencyInstance().format(p.getPrice() * item.getQuantity()));

			holder.linearLayout.setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View view)
				{
					Intent intent = new Intent(MainActivity.this, ProductDetails.class);
					intent.putExtra(getString(R.string.product_extra_key), item.getProduct());
					startActivity(intent);
				}
			});
		}

		@Override
		public int getItemCount() {
			return mDataset.size();
		}
	}

}

//api key
//AIzaSyBVVCwrSkP8PSx2nYoAvTjdfIK7dfoJ8S8