package ca.unb.mobiledev.renameme;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.SparseArray;
import android.view.MenuItem;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;

import java.io.IOException;

/**
 * ScanBarcode.java
 * <p>
 * The scan barcode activity
 */
public class ScanBarcode extends AppCompatActivity {
    /**
     * Some unique value for camera permission
     */
    private static final int REQUEST_CAMERA_PERMISSION = 201;

    /**
     * Custom view where camera image will be drawn
     */
    private SurfaceView surfaceView;

    /**
     * Camera source for scanning barcode
     */
    private CameraSource cameraSource;

    /**
     * Text instructions telling the user to scan a barcode
     */
    private TextView barcodeText;

    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        sharedPreferences = getSharedPreferences(getString(R.string.shared_cart), MODE_PRIVATE);

        super.onCreate(savedInstanceState);
        ActionBar actionBar = getSupportActionBar();
        // showing the back button in action bar
        actionBar.setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.activity_scan_barcode);
        // tie elements to the design elements
        surfaceView = findViewById(R.id.surface_view);
        barcodeText = findViewById(R.id.barcode_text);
        // initialize everything to scan a barcode
        initialiseDetectorsAndSources();
    }

    /**
     * Initialize everything to scan a new barcode
     */
    private void initialiseDetectorsAndSources() {
        // initialize detector to detect a UPC A barcode (the standard for POS systems)
        BarcodeDetector barcodeDetector = new BarcodeDetector.Builder(this)
                .setBarcodeFormats(Barcode.UPC_A)
                .build();


        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;
        // initialize the camera source, using the barcode detector as the detector
        cameraSource = new CameraSource.Builder(this, barcodeDetector)
                .setRequestedPreviewSize(height, width)
                .setAutoFocusEnabled(true)
                .build();

        // if the device has low storage it may cause issues
        IntentFilter lowStorageFilter = new IntentFilter(Intent.ACTION_DEVICE_STORAGE_LOW);
        boolean hasLowStorage = registerReceiver(null, lowStorageFilter) != null;
        if (!barcodeDetector.isOperational() && hasLowStorage) {
            // let the user know that the device has low storage
           // Toast.makeText(this, R.string.low_storage_error, Toast.LENGTH_LONG).show();
            Log.e("ScanBarcode", "not working- storage");

        } else if (!barcodeDetector.isOperational()) {
            // if the scanner isn't working for some other reason, let the user know
            //Toast.makeText(getApplicationContext(), "Scanner not operational",
                   // Toast.LENGTH_SHORT).show();
            Log.e("ScanBarcode", "not working- Unk");
        }

        // implement the callback interface to receive information about changes to the surface
        surfaceView.getHolder().addCallback(new SurfaceHolder.Callback() {
            // need created and destroyed methods to add a callback
            @Override
            public void surfaceCreated(SurfaceHolder holder) {
                // use the camera to draw the image in the surface view
                try {
                    // if the camera permission has already been granted:
                    if (ActivityCompat.checkSelfPermission(ScanBarcode.this,
                            Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                        // send camera preview frames to barcode detector
                        // and display image in the surface view
                        cameraSource.start(surfaceView.getHolder()); // potential IO exception
                    } else {
                        // request camera permissions
                        ActivityCompat.requestPermissions(ScanBarcode.this, new
                                String[]{Manifest.permission.CAMERA}, REQUEST_CAMERA_PERMISSION);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            }

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {
                cameraSource.stop();
            }
        });


        barcodeDetector.setProcessor(new Detector.Processor<Barcode>() {
            @Override
            public void release() {
            }

            @Override
            public void receiveDetections(Detector.Detections<Barcode> detections) {
                Log.i("ScanBarcode", "received detections");
                // barcodes detected
                final SparseArray<Barcode> barcodes = detections.getDetectedItems();

                // if a barcode was detected
                if (barcodes.size() != 0) {
                    ScanBarcode.this.runOnUiThread(new Runnable()
                    {
                        public void run()
                        {
                            cameraSource.stop();
                        }
                    });
                    barcodeText.post(() ->
                    {
                        // show barcode value in activity
                        String barcodeData = barcodes.valueAt(0).displayValue;

                        AsyncTask.execute(new Runnable()
                        {
                            @Override
                            public void run()
                            {
                                Log.i("ScanBarcode", "get product-> APIman");
                                Product scannedProduct = ApiManager.getProductByBarcode(ScanBarcode.this, getStoreId(), barcodeData);

                                if (scannedProduct != null)
                                {
                                    Intent openProductPage = new Intent(ScanBarcode.this,
                                            ProductDetails.class);
                                    openProductPage.putExtra(getString(R.string.product_extra_key), scannedProduct);
                                    startActivity(openProductPage);
                                } else
                                {
                                    ScanBarcode.this.runOnUiThread(new Runnable()
                                    {
                                        public void run()
                                        {
                                            initialiseDetectorsAndSources();
                                            Log.i("ScanBarcode", "bad product returned");
                                            Toast.makeText(ScanBarcode.this, "NULL product",
                                                   Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                }
                            }
                        });
                    });
                }

            }
        });
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        // don't use overridden specifics
        if (requestCode != REQUEST_CAMERA_PERMISSION) {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            return;
        }

        // permission granted, do camera permissions specifics
        if (grantResults.length != 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            try {
                // missing permission suppressed: checked with if statements
                // send camera preview frames to barcode detector
                // and display image in the surface view
                cameraSource.start(surfaceView.getHolder());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        // stop camera and release resource
        cameraSource.release();
    }

    @Override
    protected void onResume() {
        super.onResume();
        // re-initialize everything to scan a barcode
        initialiseDetectorsAndSources();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // stop camera and release resource
        cameraSource.release();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home)
        {
            this.finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public int getStoreId()
    {
        return sharedPreferences.getInt(String.valueOf(R.string.store_id), 1);
    }
}


