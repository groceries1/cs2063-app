package ca.unb.mobiledev.renameme;

public class GroceryStore
{
	private int id;
	private String name;
	private double latitude;
	private double longitude;
	private String address;
	private double distance;

	public GroceryStore(int id, String name, double latitude, double longitude, String address, double distance)
	{
		this.id = id;
		this.name = name;
		this.latitude = latitude;
		this.longitude = longitude;
		this.address = address;
		this.distance = distance;
	}

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public double getLatitude()
	{
		return latitude;
	}

	public double getLongitude()
	{
		return longitude;
	}

	public String getAddress()
	{
		return address;
	}

	public double getDistance()
	{
		return distance;
	}
}
