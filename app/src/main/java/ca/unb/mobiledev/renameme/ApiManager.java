package ca.unb.mobiledev.renameme;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.util.JsonReader;
import android.util.Log;
import android.widget.Toast;
import android.content.SharedPreferences;

import com.google.android.gms.common.api.Api;
import com.jjoe64.graphview.series.DataPoint;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

public class ApiManager
{
	static final String API_ADDRESS = "https://grocer.scd31.com/api/store/";

	// radius is in km
	public static ArrayList<GroceryStore> getStoresByDistance(Activity activity, double latitude, double longitude, double radius)
	{
		try
		{
			URL url = new URL(API_ADDRESS + "search/by_radius?latitude=" + latitude +
					"&longitude=" + longitude + "&radius=" + radius);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			JsonReader json = new JsonReader(new InputStreamReader(conn.getInputStream()));
			ArrayList<GroceryStore> stores = new ArrayList<>();
			json.beginArray();
			while(json.hasNext())
			{
				String objName = "";
				int objId = -1;
				double objLatitude = 0.0;
				double objLongitude = 0.0;
				String objAddress = "";
				double objDistance = 0.0;

				json.beginObject();
				while(json.hasNext())
				{
					String name = json.nextName();
					switch(name)
					{
						case "name":
							objName = json.nextString();
							break;
						case "id":
							objId = json.nextInt();
							break;
						case "latitude":
							objLatitude = json.nextDouble();
							break;
						case "longitude":
							objLongitude = json.nextDouble();
							break;
						case "address":
							objAddress = json.nextString();
							break;
						case "distance":
							objDistance = json.nextDouble();
							break;
					}
				}
				json.endObject();
				stores.add(new GroceryStore(objId, objName, objLatitude, objLongitude, objAddress, objDistance));
			}
			json.endArray();
			return stores;
		}
		catch(IOException e)
		{
			toastError(activity, activity.getString(R.string.internet_error));
		}

		return new ArrayList<>();
	}

	public static ArrayList<ProductPreview> searchForProduct(Activity activity, int store_id, String q)
	{

		try
		{
			Uri.Builder builder = Uri.parse(API_ADDRESS).buildUpon()
					.appendPath("" + store_id)
					.appendPath("/product/search")
					.appendQueryParameter("q", q);
			Uri uri = builder.build();
			URL url = new URL(uri.toString());
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			JsonReader json = new JsonReader(new InputStreamReader(conn.getInputStream()));
			ArrayList<ProductPreview> products = new ArrayList<>();
			json.beginArray();
			while(json.hasNext())
			{
				String objName = "";
				int objId = -1;

				json.beginObject();
				while(json.hasNext())
				{
					String name = json.nextName();
					switch(name)
					{
						case "name":
							objName = json.nextString();
							break;
						case "id":
							objId = json.nextInt();
							break;
						default:
							json.skipValue();
					}
				}
				json.endObject();
				products.add(new ProductPreview(objName, objId));
			}
			json.endArray();
			return products;
		}
		catch(IOException e)
		{
			toastError(activity, activity.getString(R.string.internet_error));
		}

		return new ArrayList<>();
	}

	public static Product getProductById(Activity activity, int store_id, int id)
	{

		try
		{
			// Safe because store_id and id are integers, so we don't have to worry about
			// padding special characters
			URL url = new URL(API_ADDRESS + store_id + "/product/id/" + id);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();

			if(conn.getResponseCode() == 404)
			{
				toastError(activity, activity.getString(R.string.product_404));
				return null;
			}

			JsonReader json = new JsonReader(new InputStreamReader(conn.getInputStream()));

			return getProductFromJsonStream(activity, json);
		}
		catch(IOException e)
		{
			toastError(activity, activity.getString(R.string.internet_error));
		}

		return null;
	}

	public static Product getProductByBarcode(Activity activity, int store_id, String barcode)
	{

		try
		{
			URL url = new URL(Uri.parse(API_ADDRESS).buildUpon()
					.appendPath("" + store_id)
					.appendPath("product/upc/")
					.appendPath(barcode)
					.build().toString());
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			if(conn.getResponseCode() == 404)
			{
				toastError(activity, activity.getString(R.string.product_404));
				return null;
			}
			JsonReader json = new JsonReader(new InputStreamReader(conn.getInputStream()));
			return getProductFromJsonStream(activity, json);
		}
		catch (IOException e)
		{
			toastError(activity, activity.getString(R.string.internet_error));
		}

		return null;
	}

	private static Product getProductFromJsonStream(Activity activity, JsonReader json)
	{
		try
		{
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

			int id = 0;
			String prodName = "";
			String description = "";
			int mass = 0; // grams
			double price = 0;
			ArrayList<DataPoint> priceHistory = new ArrayList<>();

			json.beginObject();
			while (json.hasNext())
			{
				String name = json.nextName();
				switch (name)
				{
					case "id":
						id = json.nextInt();
						break;
					case "name":
						prodName = json.nextString();
						break;
					case "description":
						description = json.nextString();
						break;
					case "mass":
						mass = json.nextInt();
						break;
					case "price":
						price = json.nextDouble();
						break;
					case "prices":
						json.beginObject();
						while (json.hasNext())
						{
							Date d = sdf.parse(json.nextName());
							priceHistory.add(new DataPoint(d, json.nextDouble()));
						}
						json.endObject();
						break;
					default:
						json.skipValue();
				}
			}
			json.endObject();

			return new Product(id, prodName, description, mass, price, priceHistory);
		}
		catch (ParseException | IOException e)
		{
			toastError(activity, activity.getString(R.string.internal_api_error));
		}

		return null;
	}

	// Toast on ui thread
	// Keeps code DRY
	private static void toastError(Activity activity, String msg)
	{
		activity.runOnUiThread(() -> Toast.makeText(activity, msg, Toast.LENGTH_LONG).show());
	}
}
