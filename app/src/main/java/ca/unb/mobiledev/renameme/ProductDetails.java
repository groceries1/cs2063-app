package ca.unb.mobiledev.renameme;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.helper.DateAsXAxisLabelFormatter;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.text.NumberFormat;
import java.util.ArrayList;

/**
 * ProductDetails.java
 *
 * <p>
 * The details activity for any given product
 */
public class ProductDetails extends AppCompatActivity {

    /**
     * The cart/shopping list to be remembered
     */
    Cart cart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        cart = new Cart(getSharedPreferences(getString(R.string.shared_cart), MODE_PRIVATE));

        ActionBar actionBar = getSupportActionBar();
        // showing the back button in action bar
        actionBar.setDisplayHomeAsUpEnabled(true);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_details);

        // tie elements to the design elements
        TextView prodNameTxt = findViewById(R.id.prodName);
        TextView unitPriceTxt = findViewById(R.id.prodPrice);
        TextView prodWeightTxt = findViewById(R.id.prodWeight);
        TextView prodDescTxt = findViewById(R.id.prodDescript);
        prodDescTxt.setMovementMethod(new ScrollingMovementMethod());
        GraphView priceGraph = findViewById(R.id.priceGraph);
        EditText qtyField = findViewById(R.id.qtyField);
        Button updateCartBtn = findViewById(R.id.update_cart_button);

        // Get the intent that started this activity
        Intent detailIntent = getIntent();
        // Get the extras from the intent
        Product product = detailIntent.getParcelableExtra(getString(R.string.product_extra_key));

        if (product != null) {
            // Fill out all the information about the item
            prodNameTxt.setText(product.getName());
            prodDescTxt.setText(product.getDescription());

            NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance();
            unitPriceTxt.setText(currencyFormatter.format(product.getPrice()));
            NumberFormat weightFormatter = NumberFormat.getNumberInstance();
            weightFormatter.setMaximumFractionDigits(3);
            String weightStr = weightFormatter.format(product.getWeight()) + " g";
            if (product.getWeight() == 0){
                weightStr = getString(R.string.weight_not_available);
            }
            prodWeightTxt.setText(weightStr);

            DateAsXAxisLabelFormatter dateAsXAxisLabelFormatter = new DateAsXAxisLabelFormatter(getApplicationContext());
            priceGraph.getGridLabelRenderer().setLabelFormatter(dateAsXAxisLabelFormatter);
            priceGraph.getGridLabelRenderer().setNumHorizontalLabels(3); // only 3 because of the space
            priceGraph.getGridLabelRenderer().setHumanRounding(false);
            priceGraph.getViewport().setScalable(true);
            ArrayList<DataPoint> priceHistory = product.getPriceHistory();
            if(priceHistory.size() > 0)
            {
                priceGraph.addSeries(new LineGraphSeries(priceHistory.toArray(new DataPoint[0])));
                priceGraph.getViewport().setMinX(priceHistory.get(0).getX());
                priceGraph.getViewport().setMaxX(priceHistory.get(priceHistory.size() - 1).getX());
                priceGraph.getViewport().setXAxisBoundsManual(true);
            }

            int inCart = cart.getQuantity(product);
            qtyField.setText(String.valueOf(inCart));

        } else {
            //null product
            prodNameTxt.setText("Null product");
            prodDescTxt.setText("Null product description");
        }

        qtyField.setOnFocusChangeListener((v, hasFocus) -> {
            if (hasFocus) {
                qtyField.setText("");
            }
        });


        if(product != null) {
            updateCartBtn.setOnClickListener(v -> {
                int newQuantity;
                try {
                  newQuantity = Integer.parseInt(qtyField.getText().toString());
                }catch (NumberFormatException nfe){
                    newQuantity = cart.getQuantity(product);
                }
                // stop focusing on the edit text
                if (qtyField.hasFocus()) {
                    qtyField.clearFocus();
                }
                hideKeyboard();
                // case with no change to cart contents
                if (newQuantity == cart.getQuantity(product)) {
                    // let the user know nothing has changed
                    Toast.makeText(ProductDetails.this, getString(R.string.cart_no_change),
                            Toast.LENGTH_SHORT).show();

                } else { // change to cart contents

                    cart.setQuantity(product, newQuantity);

                    // let the user know changes have been made
                    Toast.makeText(ProductDetails.this, getString(R.string.cart_updated),
                            Toast.LENGTH_SHORT).show();
                }
            });
        }

    }

    /**
     * Hides the soft keyboard
     */
    private void hideKeyboard() {
        Activity activity = ProductDetails.this;
        InputMethodManager imm = (InputMethodManager)
                activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = activity.getCurrentFocus();
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
