package ca.unb.mobiledev.renameme;

public class ProductPreview
{
	private String name;
	private int id;

	public ProductPreview(String name, int id)
	{
		this.name = name;
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public int getId()
	{
		return id;
	}
}
