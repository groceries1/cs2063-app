package ca.unb.mobiledev.renameme;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.common.api.Api;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.location.LocationServices;

import android.location.Location;
import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;


public class StoreSelection extends FragmentActivity
{

	final String TAG = "MapsActivity";
	SupportMapFragment supportMapFragment;
	FusedLocationProviderClient client;
	String placeType = "store";
	GoogleMap map = null;

	double currentLat = 0;
	double currentLong = 0;

	List<HashMap<String, String>> locations = new ArrayList<>();
	private RecyclerView recyclerView;
	ArrayList<GroceryStore> stores = new ArrayList<GroceryStore>();

	SharedPreferences sharedPreferences;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		sharedPreferences = getSharedPreferences(getString(R.string.shared_cart), MODE_PRIVATE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_store_selection);
		// Obtain the SupportMapFragment and get notified when the map is ready to be used.
		supportMapFragment = (SupportMapFragment) getSupportFragmentManager()
				.findFragmentById(R.id.map);

//        LocationRequest locationRequest  = LocationRequest.create()
//                .setNumUpdates(1)
//                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
//                .setInterval(1);

		client = LocationServices.getFusedLocationProviderClient(this);
		Log.i(TAG, "**************before call****************");
		getCurrentLocation();
//
//		map.setOnMarkerClickListener(e -> {
//			return true;// need to make this give lat and long and etc. to api to get store id
//		});

		//have locations now, so make recyclerview
		recyclerView = findViewById(R.id.rec_view);


	}

	private void getCurrentLocation()
	{
		if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)
		{
			client.requestLocationUpdates(null,null);

			Task<Location> task = client.getLastLocation();
			Log.i(TAG, "****************in if statement so permission was found****************");

			task.addOnSuccessListener(new OnSuccessListener<Location>()
			{
				@Override
				public void onSuccess(Location location)
				{
					if(location != null)
					{
						Log.i(TAG, "****************location was not null****************");

						currentLat = location.getLatitude();
						currentLong = location.getLongitude();

						//debug coordinates for Fredericton
//						currentLat = 45.9636;
//						currentLong = -66.6431;

						supportMapFragment.getMapAsync(new OnMapReadyCallback()
						{
							@Override
							public void onMapReady(GoogleMap googleMap)
							{
								map = googleMap;

								MarkerOptions options = new MarkerOptions().position(new LatLng(currentLat,currentLong)).title("I am there");
								options.icon(BitmapDescriptorFactory.defaultMarker(180)); //just changes the color for the current location marker
								map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(currentLat, currentLong), 10));
								map.addMarker(options);

								String url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json" +
										"?location=" + currentLat + "," + currentLong +
										"&radius=50000" + //radius in meters. 50km is the max
										"&types=" + placeType +
										"&sensor=true" +
										"&key=" + getResources().getString(R.string.google_maps_key);

								System.out.println(url);

								new PlaceTask().execute(url);

								Log.i(TAG, "****************should have placed marker****************");
							}
						});
					}
					else
					{
						Log.i(TAG, "****************location WAS null****************");
					}
				}
			});
		} else
		{
			Log.i(TAG, "****************no permission****************");
			ActivityCompat.requestPermissions(StoreSelection.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 44);
		}
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permission, @NonNull int[] grantResults)
	{
		if(requestCode == 44)
		{
			if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
				getCurrentLocation();
			}
		}
	}

	private class PlaceTask extends AsyncTask<String, Integer, String>
	{

		@Override
		protected String doInBackground(String... strings)
		{
			Log.i(TAG, "********************Entered the PlaceTask****************");
			String data = null;
			try
			{
				data = downloadUrl(strings[0]);
				stores = ApiManager.getStoresByDistance(StoreSelection.this, currentLat, currentLong, 50);

			} catch (IOException e)
			{
				e.printStackTrace();
			}

			Log.i(TAG, "*******************" + data);
			return data;
		}

		@Override
		protected void onPostExecute(String s)
		{
			new ParserTask().execute(s);
		}
	}

	private String downloadUrl(String string) throws IOException
	{
		URL url = new URL(string);

		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		connection.connect();
		InputStream stream = connection.getInputStream();
		BufferedReader reader = new BufferedReader((new InputStreamReader(stream)));
		StringBuilder builder = new StringBuilder();
		String line = "";

		while((line = reader.readLine()) != null)
		{
			builder.append(line);

		}
		String data = builder.toString();
		reader.close();
		return data;
	}

	private class ParserTask extends AsyncTask<String, Integer, List<HashMap<String, String>>>
	{

		@Override
		protected List<HashMap<String, String>> doInBackground(String... strings)
		{
			Log.i(TAG, "***************Started the ParserTask***************");
			JSonParser jSonParser = new JSonParser();
			List<HashMap<String, String>> mapList = null;
			JSONObject object = null;
			try
			{
				object = new JSONObject(strings[0]);

				mapList = jSonParser.parseResult(object);
			} catch (JSONException e)
			{
				e.printStackTrace();
			}
			return mapList;
		}

		@Override
		protected void onPostExecute(List<HashMap<String, String>> hashMaps)
		{
			//map.clear();

			for(int i = 0; i < hashMaps.size(); i++)
			{
				HashMap<String, String> hashMapList = hashMaps.get(i);
				double lat = Double.parseDouble(hashMapList.get("lat"));
				double lng = Double.parseDouble((hashMapList.get("lng")));
				String name = hashMapList.get("name");

				if(name.contains("superstore") || name.contains("Superstore"))
				{
					LatLng latLng = new LatLng(lat, lng);
					MarkerOptions options = new MarkerOptions();
					options.position(latLng);
					options.title(name);
					map.addMarker(options);
					locations.add(hashMapList);
				}
			}
			recyclerView.setAdapter(new MyAdapter(locations));

		}
	}

	public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder>
	{
		private List<HashMap<String, String>> mDataset;

		public MyAdapter(List<HashMap<String, String>> myDataset) {
			mDataset = myDataset;
		}

		// ViewHolder represents an individual item to display. In this case
		// it will just be a single TextView (displaying the title of a course)
		// but RecyclerView gives us the flexibility to do more complex things
		// (e.g., display an image and some text).
		public class ViewHolder extends RecyclerView.ViewHolder
		{
			public TextView mTextView;
            public ViewHolder(TextView v) {
				super(v);
				mTextView = v;
			}
		}

		// The inflate method of the LayoutInflater class can be used to obtain the
		// View object corresponding to an XML layout resource file. Here
		// onCreateViewHolder inflates the TextView corresponding to item_layout.xml
		// and uses it to instantiate a ViewHolder.
		@Override
		public MyAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
																	int viewType) {
			TextView v = (TextView) LayoutInflater.from(parent.getContext())
					.inflate(R.layout.item_layout_store_selection, parent, false);

			return new ViewHolder(v);
		}

		// onBindViewHolder binds a ViewHolder to the data at the specified
		// position in mDataset
		@Override
		public void onBindViewHolder(MyAdapter.ViewHolder holder, int position)
		{

			final HashMap<String, String> location = mDataset.get(position);

			String name = location.get("name");

			holder.mTextView.setText(location.get("address"));

			holder.mTextView.setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View view)
				{

					SharedPreferences.Editor editor = sharedPreferences.edit();
//					boolean found = false;

//					for(int i = 0; i < stores.size() && !found; i++){
//						for(int j = 0; j < locations.size(); j++){
//							double tempLat = stores.get(i).getLatitude();
//							double tempLng = stores.get(i).getLongitude();
//
//							double latDiff = Math.abs(tempLat - Double.parseDouble(locations.get(j).get("lat")));
//							double lngDiff = Math.abs(tempLng - Double.parseDouble(locations.get(j).get("lng")));
//
//							if(latDiff < 0.0001 && lngDiff < 0.0001){
//								editor.putInt(String.valueOf(R.string.store_id), stores.get(i).getId());
//								editor.apply();
//								found = true;
//								break;
//							}
//						}
//					}

					String text = (String) holder.mTextView.getText();
					text.toLowerCase();
					text = text.replaceAll("\\s+","");
					for(int i = 0; i < stores.size(); i++) {
						String store = stores.get(i).getAddress();
						store.toLowerCase();
						store = store.replaceAll("\\s+", "");

						if(store.substring(0, 6).equals(text.substring(0, 6))){
							editor.putInt(String.valueOf(R.string.store_id), stores.get(i).getId());
							editor.apply();
						}
					}

					Log.i(TAG, "******** " + sharedPreferences.getInt(String.valueOf(R.string.store_id), 0));
					Intent intent = new Intent(StoreSelection.this, MainActivity.class);
					startActivity(intent);
				}
			});
		}

		@Override
		public int getItemCount() {
			return mDataset.size();
		}
	}
}
