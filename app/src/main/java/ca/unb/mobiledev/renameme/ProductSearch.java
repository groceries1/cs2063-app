package ca.unb.mobiledev.renameme;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class ProductSearch extends AppCompatActivity
{
	static final String TAG = "ProductSearch";

	SharedPreferences sharedPreferences;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		sharedPreferences = getSharedPreferences(getString(R.string.shared_cart), MODE_PRIVATE);

		super.onCreate(savedInstanceState);
		ActionBar actionBar = getSupportActionBar();
		// showing the back button in action bar
		actionBar.setDisplayHomeAsUpEnabled(true);
		setContentView(R.layout.activity_product_search);

		FloatingActionButton scanButton = findViewById(R.id.scan_button);
		scanButton.setOnClickListener(v ->
		{
			Intent scanItem = new Intent(ProductSearch.this, ScanBarcode.class);
			startActivity(scanItem);
		});

		EditText searchText = findViewById(R.id.searchText);
		// Debounce code from https://stackoverflow.com/questions/12142021/how-can-i-do-something-0-5-seconds-after-text-changed-in-my-edittext-control
		searchText.addTextChangedListener(
				new TextWatcher()
				{
					private Timer timer = new Timer();
					private final long DELAY = 100;
					private int reqId = 0; // used to prevent race conditions when querying API

					@Override
					public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
					@Override
					public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

					@Override
					public void afterTextChanged(Editable editable)
					{
						timer.cancel();
						timer = new Timer();
						timer.schedule(new TimerTask()
						{
							@Override
							public void run()
							{
								int req = ++reqId;
								ArrayList<ProductPreview> products = ApiManager.searchForProduct(ProductSearch.this, getStoreId(), editable.toString());
								Log.i(TAG, "Got " + products.size() + " products");

								runOnUiThread(new Runnable()
								{
									@Override
									public void run()
									{
										if(req == reqId)
										{
											RecyclerView rv = findViewById(R.id.productView);
											rv.setAdapter(new ProductPreviewAdapter(products));
										}
									}
								});
							}
						}, DELAY);
					}
				}
		);
	}

	private class ProductPreviewAdapter extends RecyclerView.Adapter<ProductPreviewAdapter.ViewHolder>
	{
		private ArrayList<ProductPreview> mDataset;

		public ProductPreviewAdapter(ArrayList<ProductPreview> mDataset)
		{
			this. mDataset = mDataset;
		}

		// represents a single item to display
		public class ViewHolder extends RecyclerView.ViewHolder
		{
			public TextView mTextView;

			public ViewHolder(TextView v)
			{
				super(v);
				mTextView = v;
			}
		}

		@Override
		public ProductPreviewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
													   int viewType) {
			TextView v = (TextView) LayoutInflater.from(parent.getContext())
					.inflate(R.layout.product_preview_layout, parent, false);

			return new ViewHolder(v);
		}

		@Override
		public void onBindViewHolder(final ViewHolder holder, int position) {
			final ProductPreview pp = mDataset.get(position);

			holder.mTextView.setText(pp.getName());

			holder.mTextView.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v)
				{
					AsyncTask.execute(new Runnable() {
						@Override
						public void run() {
							Product p = ApiManager.getProductById(ProductSearch.this, getStoreId(), pp.getId());
							if(p != null)
							{
								Intent startIntent = new Intent(ProductSearch.this, ProductDetails.class);
								startIntent.putExtra(getString(R.string.product_extra_key), p);
								startActivity(startIntent);
							}
						}
					});

				}
			});
		}

		@Override
		public int getItemCount() {
			return mDataset.size();
		}


	}
	@Override
	public boolean onOptionsItemSelected(@NonNull MenuItem item) {
		if (item.getItemId() == android.R.id.home)
		{
			this.finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public int getStoreId()
	{
		return sharedPreferences.getInt(String.valueOf(R.string.store_id), 1);
	}
}