package ca.unb.mobiledev.renameme;

import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.HashMap;

public class Cart
{
	private static final String CART_PREFERENCES = "CART";

	public static class CartEntry
	{
		private Product product;
		private int quantity;

		public CartEntry(Product product, int quantity)
		{
			this.product = product;
			this.quantity = quantity;
		}

		public Product getProduct()
		{
			return product;
		}

		public int getQuantity()
		{
			return quantity;
		}
	}

	private static HashMap<Integer, CartEntry> entries = null; // <id, entry>
	private final SharedPreferences sharedCart;

	public Cart(SharedPreferences sharedPreferences)
	{
		sharedCart = sharedPreferences;
		if(entries == null)
		{
			String cartJson = sharedCart.getString(CART_PREFERENCES, null);
			if (cartJson == null)
			{
				entries = new HashMap<>();
			} else
			{
				entries = new Gson().fromJson(cartJson, new TypeToken<HashMap<Integer, CartEntry>>()
				{
				}.getType());
			}
		}
	}

	public void setQuantity(Product p, int quantity)
	{
		if(quantity == 0)
		{
			entries.remove(p.getId());
		}
		else
		{
			entries.put(p.getId(), new CartEntry(p, quantity));
		}

		// Save to shared preferences
		SharedPreferences.Editor editor = sharedCart.edit();
		editor.putString(CART_PREFERENCES, new Gson().toJson(entries));
		editor.apply();
	}

	public int getQuantity(Product p)
	{
		CartEntry entry = entries.get(p.getId());
		if(entry == null)
		{
			return 0;
		}
		else
		{
			return entry.getQuantity();
		}
	}

	public ArrayList getEntries()
	{
		return new ArrayList(entries.values());
	}

	public double totalPrice()
	{
		double total = 0.0;
		for(CartEntry entry : entries.values())
		{
			total += entry.getQuantity() * entry.getProduct().getPrice();
		}

		return total;
	}

	public int totalMass()
	{
		int total = 0;
		for(CartEntry entry : entries.values())
		{
			total += entry.getQuantity() * entry.getProduct().getWeight();
		}

		return total;
	}
}