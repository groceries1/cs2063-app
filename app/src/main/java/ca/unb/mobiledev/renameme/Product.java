package ca.unb.mobiledev.renameme;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;

import com.jjoe64.graphview.series.DataPoint;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Product.java
 *
 * <p>
 * This class represents a grocery product.
 * A product object is used to store information retrieved from the API about a product,
 * and pass information between activities.
 */
public class Product implements Parcelable {

    /**
     * Unique product ID
     */
    private final int id;

    /**
     * Name of the product
     */
    private final String name;

    /**
     * Description of the product
     */
    private final String description;

    /**
     * Wight of the product in grams (per one product unit)
     */
    private final int weight;

    /**
     * Price per one unit of the product
     */
    private final double price;

    /**
     * Price history of product price per one unit
     */
    private final ArrayList<DataPoint> priceHistory;

    /**
     * Constructs a new Product object
     *
     * @param name        The name of the product
     * @param description The product's description
     * @param weight      The weight of the product in grams
     * @param price       The price per unit
     */
    public Product(int id, String name, String description, int weight, double price,
                   ArrayList<DataPoint> priceHistory) {
        //sort the data points according to date
        Collections.sort(priceHistory, (DataPoint d1, DataPoint d2) -> (int) (d1.getX() - d2.getX()));
        this.id = id;
        this.name = name;
        this.description = description;
        this.weight = weight;
        this.price = price;
        this.priceHistory = priceHistory;
    }

    /**
     * Construct a product object from a parcel
     *
     * @param in The parcel
     */
    @SuppressWarnings("unchecked")
    protected Product(Parcel in) {
        id = in.readInt();
        name = in.readString();
        description = in.readString();
        weight = in.readInt();
        price = in.readDouble();
        Bundle bundle = in.readBundle(getClass().getClassLoader());
        priceHistory = (ArrayList<DataPoint>) bundle.getSerializable("priceHistoryMap");
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeString(description);
        dest.writeInt(weight);
        dest.writeDouble(price);
        Bundle bundle = new Bundle();
        bundle.putSerializable("priceHistoryMap", priceHistory);
        dest.writeBundle(bundle);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    // for parcelable objects
    public static final Creator<Product> CREATOR = new Creator<Product>() {
        @Override
        public Product createFromParcel(Parcel in) {
            return new Product(in);
        }

        @Override
        public Product[] newArray(int size) {
            return new Product[size];
        }
    };


    /**
     * Retrieve the unique product ID
     *
     * @return Unique product ID
     */
    public int getId() {
        return id;
    }

    /**
     * Retrieves the product's price per unit
     *
     * @return Price per one unit of the product
     */
    public double getPrice() {
        return price;
    }

    /**
     * Retrieves the product's weight in grams (per unit)
     *
     * @return Weight in grams per one unit of the product
     */
    public int getWeight() {
        return weight;
    }

    /**
     * Retrieves the product description
     *
     * @return Product description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Retrieves the name of the product
     *
     * @return Product name
     */
    public String getName() {
        return name;
    }


    /**
     * Retrieves the product's price history
     *
     * @return Price history
     */
    public ArrayList<DataPoint> getPriceHistory() {
        return priceHistory;
    }

}
